# -*- coding: utf-8  -*-

import pdb

import string
import sys
import io
import time
from re import search, split, match, sub
import re

sys.path.insert(0, 'pywikipedia')
sys.path.insert(0, '.')

del sys

import wikipedia, config

re_header = re.compile( ur"^=.*=$", re.MULTILINE)

def check(art):
    global site, headers, log
    page = wikipedia.Page(site, art)
    if not page.exists():
        print u'- ' + art
        return
    else:
        print u'+ ' + art
    body = page.get()
    for m in re_header.findall(body):
        while re_header.match(m): m = m[1:-1]
        m = m.strip()
        m = m.replace(art, u"YYYY")
        if m not in headers:
            print m
            headers[m] = {'count': 0, 'example': u"[[%s]]" % art}
        headers[m]['count'] += 1
        if headers[m]['count'] in range(2, 6):
            headers[m]['example'] += u", [[%s]]" % art

def main():
    global site, headers
    try:
        headers = dict()
        wikipedia.handleArgs()
        site = wikipedia.getSite()
        for y in range(1, 2501):
            check(str(y))
        for y in range(1, 2001):
            check("%d p.n.e." % y)
    finally:
        for h in headers:
            log.write(u"%5d:\t%s\t%s\n" % (headers[h]['count'], h, headers[h]['example']))

if __name__ == "__main__":
    try:
        log = io.open('naglowki.txt', 'w')
        main()
    finally:

        wikipedia.stopme()
        log.close()
