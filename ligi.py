# -*- coding: utf-8  -*-

import pdb

import string
import sys
import io
import time
from re import search, split, match, sub
import re

sys.path.insert(0, 'pywikipedia')
sys.path.insert(0, '.')

del sys

import wikipedia, config

FINDER = re.compile(
    #ur"\{\{col-begin\}\}((?:\s*\{\{(?:fb|hb|vb) uczestnicy[^}]+\}\})+)\s*\{\{col-break\}\}\s*((?:\{\{(?:fb|hb|vb) map[^}]+\}\}\s*)+)\{\{col-end\}\}",
    ur"\{\{col-begin\}\}((?:\{\{(?:fb|hb|vb) uczestnicy[^}]+\}\}|\{\{trofeum[^}]+\}\}|[^{])+)\s*\{\{col-break\}\}\s*((?:\{\{(?:fb|hb|vb) map[^}]+\}\}\s*)+)\{\{col-end\}\}",
    re.DOTALL | re.IGNORECASE)

def sub_match(m):
    m_body, m_map = map(lambda i: m.group(i).strip(), [1, 2])
    #if re.search(ur"\{\{-\}\}$", m_body) == None:
    #    m_body += u"\n{{-}}"
    return u"\n".join([m_map, m_body])

def fix(old):
    new = FINDER.sub(sub_match, old)
    if new == old:
        raise Exception("Nothing changed")
    return new

def run(page):
    global log
    if page.title().find(':') < 0:
        old = page.get()
        try:
            new = fix(old)
            time.sleep(1)
            page.put(new,  maxTries=1, comment=u"Robot usuwa niepotrzebny szablon {{col-break}} – szablon {{fb map}} sam pozycjonuje się po prawej stronie.")
            msg = u"DONE:  %s" % page.title()
        except Exception as ex:
            msg = u"FAIL:  %s %s" % (page.title().ljust(25, '.'), unicode(ex))
    else:
        msg = u"OMIT:  %s" % page.title()
    print msg
    log.write(msg + '\n')

def main():
    global log
    try:
        log = io.open('ligi.txt', 'w')

        wikipedia.handleArgs()
        site = wikipedia.getSite()

        col_break = wikipedia.Page(site, u"Szablon:col-break")
        col_break_refs = [page for page in col_break.getReferences()]

        intersection = []
        for sport in ['fb']: #'fb', 'vb'
            fb_map = wikipedia.Page(site, u"Szablon:%s map" % sport)
            intersection += [page for page in fb_map.getReferences() if page in col_break_refs]

        print "%d articles to go" % len(intersection)
        for page in intersection:
            run(page)
    finally:
        log.close()

if __name__ == "__main__":
    try:
        main()
    finally:
        wikipedia.stopme()
