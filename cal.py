# -*- coding: utf-8  -*-

import pdb

import string
import sys
import io
import time
from re import search, split, match, sub
import re

sys.path.insert(0, 'pywikipedia')
sys.path.insert(0, '.')

del sys

import wikipedia, config

def main():
    global operations, log, unrecognized
    arr = [u"poniedziałku", u"wtorku", u"środy", u"czwartku", u"piątku", u"soboty", u"niedzieli"]
    tgarr = [u"kalendarz na rok 1582"]
    for d in arr:
        tgarr.append(u"kalendarz od " + d)
        tgarr.append(u"kalendarz od " + d + u" przestępny")

    wikipedia.handleArgs()
    site = wikipedia.getSite()

    pages = (wikipedia.Page(site, u"szablon:" + t) for t in tgarr)
    for tpl in pages:
        print tpl.title()
        body = tpl.get()
        repl = sub(u'<table style="float:center;', u'<table style="', body)
        if repl != body:
            tpl.put(repl, maxTries=1, comment=u"Robot poprawia: nie ma czegoś takiego w CSS jak float:center, a psuje to wyświetlanie w Operze.")

if __name__ == "__main__":
    try:
        main()
    finally:
        wikipedia.stopme()


